from aiogram.utils import executor
from aiogram.utils.markdown import hbold, hcode
from aiogram import Bot, types
from aiogram.dispatcher import Dispatcher
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from aiogram.dispatcher.filters.state import State, StatesGroup
from services import db_worker as db
from services import print_tiket as printers
from services import pdf_creator as pdf
from services import convertor as convert
from services import keyboard as kb
import tempfile
import json


with open(f'config.json') as f:
    read_data = json.load(f)
    bot_token = read_data["bot_token"]
    micro_qr_print = read_data["micro_qr_print"]
    super_administrator = read_data["super_administrator_id"]
    if micro_qr_print == "True":
        micro_qr_print = True
    else:
        micro_qr_print = False


storage = MemoryStorage()
bot = Bot(token=bot_token, parse_mode=types.ParseMode.HTML)
dp = Dispatcher(bot, storage=storage)

class Print_state(StatesGroup):
    quantity = State()
    search = State()
    search_res = State()
    quantity = State()
    select_printer = State()
    print = State()

class Command_state(StatesGroup):
    help = State()
    add_cons = State()
    add_user = State()
    del_user = State()


class Add_podr_state(StatesGroup):
    pord_id = State()
    printer_type = State()
    printer_ip = State()
    printer_smb_name = State()
    printer_cred = State()
    finish = State()


@dp.callback_query_handler(text_contains="get_main_menu", state='*')
async def inline_get_main_menu(callback: types.CallbackQuery):
    await Print_state.search.set()
    await bot.answer_callback_query(callback.id, text=' ')
    if callback.message.chat.id in db.get_users():
        user_podr = json.loads(db.get_user_info(callback.message.chat.id)[0][6].replace("'", "\""))['podr']
        await callback.message.answer(f"✏️ Напишите {hbold('имя')} и {hbold('фамилия')} или {hbold('штрих-код')} консультанта которого хотите найти\n\n" \
                                      f"ℹ️ Так же можно отправить {hbold('голосовое сообщение')} для поиска или {hbold('фото этикетки')} с qr кодом\n" \
                                      f"ℹ️ Доступные Вам подразделения: {user_podr}", 
                                      reply_markup=kb.get_main_menu_il())
    else:
        await callback.message.answer(f"⚔️ Простите, у Вас нет доступа к боту. Чтобы получить доступ напишите /help", reply_markup=types.ReplyKeyboardRemove())


@dp.message_handler(commands='help', state='*')
async def add_cons(message: types.Message):
    if message.chat.id in db.get_users():
        await Command_state.help.set()
        await message.answer(f"✏️ Напишите сообщение для администратора для получения поддержки", reply_markup=kb.get_main_menu_il())
    else:
        await Command_state.help.set()
        await message.answer(f"ℹ️ Ваш telegram_id - {hcode(message.chat.id)}\n\n" \
                             f"✏️ Пожалуйста напишите сообщение администратору чтобы добавил Вам доступ\n\n" \
                             f"ℹ️ Например:\n Я являюсь менеджером в {hbold('(Указать место)')}, у меня telegram_id - 123456789, прошу дать доступ в подразделения {hcode('[000, 001]')}", 
                             reply_markup=types.ReplyKeyboardRemove())


@dp.message_handler(commands='start', state='*')
async def non_state(message: types.Message):
    if message.chat.id in db.get_users():
        user_podr = json.loads(db.get_user_info(message.chat.id)[0][6].replace("'", "\""))['podr']
        await message.answer(f"✏️ Напишите {hbold('имя')} и {hbold('фамилия')} или {hbold('штрих-код')} консультанта которого хотите найти\n\n" \
                             f"ℹ️ Так же можно отправить {hbold('голосовое сообщение')} для поиска или {hbold('фото этикетки')} с qr кодом\n" \
                             f"ℹ️ Доступные Вам подразделения: {user_podr}", 
                                reply_markup=kb.get_main_menu_il())
        await Print_state.search.set()
    else:
        await message.answer("⚔️ Простите, у Вас нет доступа к боту. Чтобы получить доступ напишите /help", reply_markup=types.ReplyKeyboardRemove())


@dp.message_handler(commands='add_cons', state='*')
async def add_cons(message: types.Message):
    if message.chat.id in db.get_users():
        await Command_state.add_cons.set()
        await message.answer(f"✏️ Напишите через пробел:\n" \
                             f"{hbold('имя')} {hbold('фамилия')} {hbold('штрих-код')} {hbold('подразделение')}\n\n" \
                             f"Например:\n" \
                             f"{hcode('Иванов Иван 2090000000000 107')}", 
                             reply_markup=kb.get_main_menu_il())
    else:
        await message.answer(f"⚔️ Простите, у Вас нет доступа к боту", reply_markup=types.ReplyKeyboardRemove())


@dp.message_handler(commands='add_user', state='*')
async def add_cons(message: types.Message):
    if message.chat.id in db.get_admins():
        await Command_state.add_user.set()
        await message.answer(f"✏️ Напишите через пробел:\n" \
                             f"{hbold('имя')} {hbold('telegram_id')} # [{hbold('доступ к подразделениям')}]\n\n" \
                             f"Например:\n" \
                             f"{hcode('12345678 # [107, 168]')}", 
                             reply_markup=kb.get_main_menu_il())
    elif message.chat.id in db.get_users():
        await message.answer(f"⚔️ Простите, у Вас нет администраторских прав для добавления пользователей", 
                             reply_markup=types.ReplyKeyboardRemove())
    else:
        await message.answer("⚔️ Простите, у Вас нет доступа к боту. Чтобы получить доступ напишите /help", reply_markup=types.ReplyKeyboardRemove())


@dp.message_handler(commands='del_user', state='*')
async def add_cons(message: types.Message):
    if message.chat.id in db.get_admins():
        await Command_state.del_user.set()
        await message.answer(f"✏️ Напишите telegram_id пользователя для удаления", 
                             reply_markup=kb.get_main_menu_il())
    elif message.chat.id in db.get_users():
        await message.answer(f"⚔️ Простите, у Вас нет администраторских прав для удаления пользователей", 
                             reply_markup=types.ReplyKeyboardRemove())
    else:
        await message.answer("⚔️ Простите, у Вас нет доступа к боту. Чтобы получить доступ напишите /help", reply_markup=types.ReplyKeyboardRemove())


@dp.message_handler(commands='add_podr', state='*')
async def add_cons(message: types.Message):
    if message.chat.id in db.get_admins():
        await Add_podr_state.pord_id.set()
        await message.answer(f"✏️ Напишите номер подразделения которую хотите добавить", 
                             reply_markup=kb.get_main_menu_il())
    elif message.chat.id in db.get_users():
        await message.answer(f"⚔️ Простите, у Вас нет администраторских прав для добавления подразделения", 
                             reply_markup=types.ReplyKeyboardRemove())
    else:
        await message.answer("⚔️ Простите, у Вас нет доступа к боту. Чтобы получить доступ напишите /help", reply_markup=types.ReplyKeyboardRemove())


@dp.message_handler(state=Command_state.help)
async def add_user(message: types.Message):
    appeal_data = message.text
    if message.chat.id in db.get_users():
        await bot.send_message(chat_id=super_administrator, text=f"Пользователь:\nTelegram_id {message.from_user.id}\n" \
                                                       f"Имя {message.from_user.full_name}\n" \
                                                       f"Пишет:\n{appeal_data}", reply_markup=kb.get_main_menu_il())
        await message.answer(f"✅ Обращение отправлено администратору!", 
                             reply_markup=types.ReplyKeyboardRemove()) 
    else:
        await bot.send_message(chat_id=super_administrator, text=f"Пользователь:\nTelegram_id {message.from_user.id}\n" \
                                                       f"Имя {message.from_user.full_name}\n" \
                                                       f"Хочет получить доступ!\n" \
                                                       f"Пишет:\n{appeal_data}", reply_markup=kb.get_main_menu_il())
        await message.answer(f"✅ Заявка отправлена администратору, попробуйте написать /start через какое то время для проверки доступа", 
                             reply_markup=types.ReplyKeyboardRemove()) 


@dp.message_handler(state=Command_state.add_cons)
async def add_user(message: types.Message):
    if message.chat.id in db.get_users():
        user_podr = json.loads(db.get_user_info(message.chat.id)[0][6].replace("'", "\""))['podr']
        entered_data = message.text.split(' ')
        if len(entered_data) == 4:
            firsname = entered_data[0].strip()
            lastname = entered_data[1].strip()
            qr = entered_data[2].strip()
            podr = entered_data[3].strip()
            if qr.isdigit() and podr.isdigit():
                if int(podr) in user_podr:
                    add_res = db.add_cons(firsname, lastname, qr, podr)
                    if add_res == "Added":
                        await message.answer(f"✅ Данные успешно занесены в базу.\n\n" \
                                            f"✏️ Напишите:\n{hbold('имя')} {hbold('фамилия')} {hbold('штрих-код')} {hbold('подразделение')}", 
                                            reply_markup=kb.get_main_menu_il())
                    elif add_res == "Updated":
                        await message.answer(f"✅ В базе данных уже был этот штрих код в этом подразделении, данные по ниму обнавлены.\n\n" \
                                            f"✏️ Напишите:\n{hbold('имя')} {hbold('фамилия')} {hbold('штрих-код')} {hbold('подразделение')}", 
                                            reply_markup=kb.get_main_menu_il())
                    else:
                        await message.answer(f"❌ Что то не так при добавлении...\n\n" \
                                            f"✏️ Напишите:\n{hbold('имя')} {hbold('фамилия')} {hbold('штрих-код')} {hbold('подразделение')}", 
                                            reply_markup=kb.get_main_menu_il())
                else:
                    await message.answer(f"❌ Простите, у вас нет доступа к подразделению {podr}\n\n" \
                                         f"✏️ Напишите:\n{hbold('имя')} {hbold('фамилия')} {hbold('штрих-код')} {hbold('подразделение')}", 
                                         reply_markup=kb.get_main_menu_il())
        else:
            await message.answer(f"✏️ Напишите:\n{hbold('имя')} {hbold('фамилия')} {hbold('штрих-код')} {hbold('подразделение')}", 
                                 reply_markup=kb.get_main_menu_il())
    else:
        await message.answer("⚔️ Простите, у Вас нет доступа к боту. Чтобы получить доступ напишите /help", reply_markup=types.ReplyKeyboardRemove())


@dp.message_handler(state=Command_state.add_user)
async def add_user(message: types.Message):
    if message.chat.id in db.get_admins():
        entered_data = [i.strip() for i in message.text.split('#')]
        if len(entered_data) == 2:
            tg_id = entered_data[0]
            available_podr = entered_data[1]
            db_podr = db.get_podr_id()
            if '[' in available_podr and ']' in available_podr:
                if ',' in available_podr:
                    av_podr_list = [int(p.strip()) for p in available_podr.replace('[', '').replace(']', '').split(',')]
                else:
                    av_podr_list = [int(available_podr.replace('[', '').replace(']', ''))]
                check_podr = True
                for p in av_podr_list:
                    if p not in db_podr:
                        check_podr = False
                if check_podr:
                    add_res = db.add_user(tg_id, "{" + "'podr': " + available_podr + "}")
                    if add_res == "Added":
                        await message.answer(f"✅ Пользователь добавлен!", reply_markup=kb.get_main_menu_il()) 
                    elif add_res == "Updated":
                        await message.answer(f"✅ В базе уже был пользователь с этим telegram_id, данные по нему поправлены", 
                                            reply_markup=kb.get_main_menu_il()) 
                    else:
                        await message.answer(f"❌ что то не так при добавлении пользователя", 
                                            reply_markup=kb.get_main_menu_il())
                else:
                    await message.answer(f"❌ Какого то указанного в списке подразделения нет в боте, обратитесь к администратору (/help)\n\n" \
                                         f"ℹ️ Напишите ему какую команду для добавления вводите", 
                                         reply_markup=kb.get_main_menu_il())
            else:
                await message.answer(f"✏️ Напишите {hbold('правильно')} через пробел:\n" \
                                     f"{hbold('имя')} {hbold('telegram_id')} # [{hbold('доступ к подразделениям')}]\n\n" \
                                     f"Например:\n" \
                                     f"{hcode('12345678 # [107, 168]')}", 
                                     reply_markup=kb.get_main_menu_il())  
        elif len(entered_data) == 3 and entered_data[2] == 'True' or entered_data[2] == 'False':
            tg_id = entered_data[0]
            available_podr = entered_data[1]
            is_admin = entered_data[2]
            if is_admin == 'True':
                is_admin == True
            else:
                is_admin == False
            if '[' in available_podr and ']' in available_podr:
                add_res = db.add_user(tg_id, "{" + "'podr': " + available_podr + "}", is_admin=is_admin)
                if add_res == "Added":
                    await message.answer(f"✅ Пользователь добавлен!", reply_markup=kb.get_main_menu_il()) 
                elif add_res == "Updated":
                    await message.answer(f"✅ В базе уже был пользователь с этим telegram_id, данные по нему поправлены", 
                                         reply_markup=kb.get_main_menu_il()) 
                else:
                    await message.answer(f"❌ что то не так при добавлении пользователя", 
                                         reply_markup=kb.get_main_menu_il())
            else:
                await message.answer(f"✏️ Напишите {hbold('правильно')} через пробел:\n" \
                             f"{hbold('имя')} {hbold('telegram_id')} # [{hbold('доступ к подразделениям')}]\n\n" \
                             f"Например:\n" \
                             f"{hcode('12345678 # [107, 168]')}",
                             reply_markup=kb.get_main_menu_il())
        else:
            await message.answer(f"✏️ Напишите {hbold('правильно')} через пробел:\n" \
                             f"{hbold('имя')} {hbold('telegram_id')} # [{hbold('доступ к подразделениям')}]\n\n" \
                             f"Например:\n" \
                             f"{hcode('12345678 # [107, 168]')}", 
                             reply_markup=kb.get_main_menu_il())
    elif message.chat.id in db.get_users():
        await message.answer(f"⚔️ Простите, у Вас нет администраторских прав для добавления пользователя", 
                             reply_markup=types.ReplyKeyboardRemove())
    else:
        await message.answer("⚔️ Простите, у Вас нет доступа к боту. Чтобы получить доступ напишите /help", reply_markup=types.ReplyKeyboardRemove())


@dp.message_handler(state=Command_state.del_user)
async def add_user(message: types.Message):
    if message.chat.id in db.get_admins():
        if message.text.isdigit():
            all_users = db.get_users()
            if int(message.text) in all_users:
                db.del_user(int(message.text))
                await message.answer(f"✅ Пользователь удален!", reply_markup=kb.get_main_menu_il()) 
            else:
                await message.answer(f"🤷‍♂️ Простите, пользователя с таким telegram_id нет в базе", reply_markup=kb.get_main_menu_il()) 
        else:
            await message.answer(f"🤷‍♂️ Пожалуйста напишите правильно telegram_id пользователя для удаления", reply_markup=kb.get_main_menu_il())
    elif message.chat.id in db.get_users():
        await message.answer(f"⚔️ Простите, у Вас нет администраторских прав для удаления пользователя", 
                             reply_markup=types.ReplyKeyboardRemove())
    else:
        await message.answer("⚔️ Простите, у Вас нет доступа к боту. Чтобы получить доступ напишите /help", reply_markup=types.ReplyKeyboardRemove())


@dp.message_handler(state=Add_podr_state.pord_id)
async def add_podr_id(message: types.Message):
    if message.chat.id in db.get_admins():
        if message.text.isdigit():
            if len(message.text) == 3:
                db.tmp_update_podr_id(message.chat.id, int(message.text))
                await Add_podr_state.printer_type.set()
                await message.answer(f"✏️ Выбирите тип принтера", 
                                     reply_markup=kb.printer_type())
            else:
                await message.answer(f"✏️ Напишите правильно номер подразделения которую хотите добавить", 
                                     reply_markup=kb.get_main_menu_il())
        else:
            await message.answer(f"✏️ Напишите правильно номер подразделения которую хотите добавить", 
                                 reply_markup=kb.get_main_menu_il())
    elif message.chat.id in db.get_users():
        await message.answer(f"⚔️ Простите, у Вас нет администраторских прав для добавления подразделения", 
                             reply_markup=types.ReplyKeyboardRemove())
    else:
        await message.answer("⚔️ Простите, у Вас нет доступа к боту. Чтобы получить доступ напишите /help", reply_markup=types.ReplyKeyboardRemove())


@dp.message_handler(state=Add_podr_state.printer_type)
async def add_podr_printer_type(message: types.Message):
    if message.chat.id in db.get_admins():
        if message.text == "Citizen":
            db.tmp_update_printer_type(message.chat.id, "Citizen")
            await message.answer(f"✏️ Напишите IP адрес компьютера через который подключен принтер", 
                                 reply_markup=kb.get_main_menu_il())
            await Add_podr_state.printer_ip.set()
        elif message.text == "Zebra":
            db.tmp_update_printer_type(message.chat.id, "Zebra")
            await message.answer(f"✏️ Напишите IP адрес принтера", 
                                 reply_markup=kb.get_main_menu_il())
            await Add_podr_state.printer_ip.set()
        else:
            await message.answer(f"✏️ Выбирите принтер из кнопок внизу...", 
                                 reply_markup=kb.printer_type())
    elif message.chat.id in db.get_users():
        await message.answer(f"⚔️ Простите, у Вас нет администраторских прав для добавления подразделения", 
                             reply_markup=types.ReplyKeyboardRemove())
    else:
        await message.answer("⚔️ Простите, у Вас нет доступа к боту. Чтобы получить доступ напишите /help", reply_markup=types.ReplyKeyboardRemove())


@dp.message_handler(state=Add_podr_state.printer_ip)
async def add_podr_printer_ip(message: types.Message):
    if message.chat.id in db.get_admins():
        entered_data = message.text
        check_data = entered_data.split('.')
        test_ip = True
        printer_type = db.tmp_get_printer_type(message.chat.id)
        podr = db.tmp_get_podr_id(message.chat.id)
        for c in check_data:
            if not c.isdigit():
                test_ip = False
        if test_ip and len(check_data ) == 4:
            db.tmp_update_printer_ip(message.chat.id, message.text)
            if printer_type == "Citizen":
                printer_ip = db.tmp_get_printer_ip(message.chat.id)
                await Add_podr_state.printer_smb_name.set()
                await message.answer(f"ℹ️ На компьютере {printer_ip} нужно расшарить имя принтера по образцу:\n" \
                                     f"{hbold('город')} - {hbold('улица дом')} - {hbold('места')}\n\n" \
                                     f"ℹ️ При подключении на Windows машины от Linux машин, в Windows машине пересоздается новый принтер, старый отключается, " \
                                     f"так как подключаются Linux`овые драйверы. На Windows машине нужно удалить старое устройство и назвать новое устройство как старое было.\n\n" \
                                     f"✏️ Напишите имя расшаренного принтера\n\n" \
                                     f"ℹ️ Например {hcode('kzn-p2-managers')}\n", 
                                     reply_markup=kb.get_main_menu_il())
            else:
                await message.answer(f"✏️ Напишите имя принтера по образцу {hbold('город')} - {hbold('улица дом')} - {hbold('места')}\n\n" \
                                     f"ℹ️ Например {hcode('kzn-p2-managers')}\n", 
                                     reply_markup=kb.get_main_menu_il())
                await Add_podr_state.printer_smb_name.set()
        else:
            await message.answer(f"✏️ Напишите правильный ip адрес", 
                                 reply_markup=kb.get_main_menu_il())
            
    elif message.chat.id in db.get_users():
        await message.answer(f"⚔️ Простите, у Вас нет администраторских прав для добавления подразделения", 
                             reply_markup=types.ReplyKeyboardRemove())
    else:
        await message.answer("⚔️ Простите, у Вас нет доступа к боту. Чтобы получить доступ напишите /help", reply_markup=types.ReplyKeyboardRemove())


@dp.message_handler(state=Add_podr_state.printer_smb_name)
async def add_podr_printer_smb_name(message: types.Message):
    if message.chat.id in db.get_admins():
        db.tmp_update_printer_smb_name(message.chat.id, message.text)
        entered_data = message.text
        printer_type = db.tmp_get_printer_type(message.chat.id)
        printer_ip = db.tmp_get_printer_ip(message.chat.id)
        printer_name = db.tmp_get_printer_smb_name(message.chat.id)
        podr = db.tmp_get_podr_id(message.chat.id)
        if printer_type == "Citizen":
            await Add_podr_state.printer_cred.set()
            await message.answer(f"ℹ️ Нужны креды для компьютера {printer_ip} для подключения по smb\n" \
                                f"Например {hcode('hafizov_a:password')}\n\n" \
                                f"✏️ Напишите данные для подключения по smb\n", 
                                reply_markup=kb.get_main_menu_il())
        else:
            await message.answer(f"Подразделение {hbold(podr)}\n" \
                                 f"Принтер {hbold(printer_type)}\n" \
                                 f"Название принтера {hbold(printer_name)}\n" \
                                 f"IP адрес {hbold(printer_ip)}\n\n" \
                                 f"Подключаем?", 
                                 reply_markup=kb.printer_yes_no())
            await Add_podr_state.finish.set()
    elif message.chat.id in db.get_users():
        await message.answer(f"⚔️ Простите, у Вас нет администраторских прав для добавления подразделения", 
                             reply_markup=types.ReplyKeyboardRemove())
    else:
        await message.answer("⚔️ Простите, у Вас нет доступа к боту. Чтобы получить доступ напишите /help", reply_markup=types.ReplyKeyboardRemove())


@dp.message_handler(state=Add_podr_state.printer_cred)
async def add_podr_printer_ip(message: types.Message):
    if message.chat.id in db.get_admins():
        printer_type = db.tmp_get_printer_type(message.chat.id)
        printer_ip = db.tmp_get_printer_ip(message.chat.id)
        printer_smb_name = db.tmp_get_printer_smb_name(message.chat.id)
        podr = db.tmp_get_podr_id(message.chat.id)
        entered_data = message.text.split(':')
        if len(entered_data) == 2:
            await message.delete()
            await message.answer(f"Подразделение {hbold(podr)}\n" \
                                 f"Принтер {hbold(printer_type)}\n" \
                                 f"SMB имя принтера {hbold(printer_smb_name)}\n" \
                                 f"Пользователь smb {hbold(entered_data[0])}\n" \
                                 f"Подключаем?", 
                                 reply_markup=kb.printer_yes_no())
            db.tmp_update_printer_cred(message.chat.id, f'{entered_data[0]}:{entered_data[1]}')
            await Add_podr_state.finish.set()
        else:
            await message.delete()
            await message.answer(f"ℹ️ Нужно правильно прописать креды для компьютера {printer_ip} для подключения по smb\n\n" \
                                 f"✏️ Напишите login:password для подключения по smb\n\n" \
                                 f"ℹ️ Например {hcode('hafizov_a:password')}", 
                                 reply_markup=kb.get_main_menu_il())
    elif message.chat.id in db.get_users():
        await message.answer(f"⚔️ Простите, у Вас нет администраторских прав для добавления подразделения", 
                             reply_markup=types.ReplyKeyboardRemove())
    else:
        await message.answer("⚔️ Простите, у Вас нет доступа к боту. Чтобы получить доступ напишите /help", reply_markup=types.ReplyKeyboardRemove())


@dp.message_handler(state=Add_podr_state.finish)
async def add_podr_printer_ip(message: types.Message):
    if message.chat.id in db.get_admins():
        podr = db.tmp_get_podr_id(message.chat.id)
        printer_type = db.tmp_get_printer_type(message.chat.id)
        printer_ip = db.tmp_get_printer_ip(message.chat.id)
        printer_name = db.tmp_get_printer_smb_name(message.chat.id)
        printer_cred = db.tmp_get_printer_cred(message.chat.id)
        if message.text == "✅ Да":           
            res_add = db.create_podr(podr, printer_type, printer_ip, printer_name)
            if res_add == "Added":
                await bot.send_message(chat_id=super_administrator, text=f"Нужно добавить принтер на сервере!\n" \
                                                                         f"Подразделение {podr}\n" \
                                                                         f"Производитель {printer_type}\n" \
                                                                         f"IP адрес {printer_ip}\n" \
                                                                         f"Имя принтера {printer_name}\n" \
                                                                         f"Креды {printer_cred}", 
                                        reply_markup=kb.get_main_menu_il()) 
                db.tmp_pord_data_clear(message.chat.id)
                await Add_podr_state.pord_id.set()
                await message.answer(f"✅ Подразделение добавлена!\n\n" \
                                    f"Ждите пока администратор бота добавит принтер в Cups\n\n" \
                                    f"✏️ Напишите номер подразделения если хотите добавить еще одну", 
                                    reply_markup=kb.get_main_menu_il())
            elif res_add == "Updated":
                db.tmp_pord_data_clear(message.chat.id)
                await bot.send_message(chat_id=super_administrator, text=f"Нужно поправить данные о принтере!\n" \
                                                                         f"Подразделение {podr}\n" \
                                                                         f"Производитель {printer_type}\n" \
                                                                         f"IP адрес {printer_ip}\n" \
                                                                         f"Имя принтера {printer_name}\n" \
                                                                         f"Креды {printer_cred}",
                                        reply_markup=kb.get_main_menu_il())
                await Add_podr_state.pord_id.set()
                await message.answer(f"✅ В базе данных уже быдо это подразделение, данные по принтеру исправлены\n\n" \
                                     f"Ждите пока администратор бота поправит данные о принтере в Cups\n\n" \
                                     f"✏️ Напишите номер подразделения если хотите добавить еще одну", 
                                    reply_markup=kb.get_main_menu_il())
        elif message.text == "❌ Нет":    
            db.tmp_pord_data_clear(message.chat.id)
            await Add_podr_state.pord_id.set()
            await message.answer(f"✏️ Напишите номер подразделения которую хотите добавить", 
                             reply_markup=kb.get_main_menu_il())
        else:
            if printer_type == "Citizen":
                await message.answer(f"Подразделение {hbold(podr)}\n" \
                                     f"Принтер {hbold(printer_type)}\n" \
                                     f"SMB имя принтера {hbold(printer_name)}\n" \
                                     f"Пользователь smb {hbold(printer_cred.split(':')[0])}\n" \
                                     f"Подключаем?", 
                                     reply_markup=kb.printer_yes_no())
            else:
                await message.answer(f"Подразделение {hbold(podr)}\n" \
                                     f"Принтер {hbold(printer_type)}\n" \
                                     f"Название принтера {hbold(printer_name)}\n" \
                                     f"IP адрес {hbold(printer_ip)}\n\n" \
                                     f"Подключаем?", 
                                     reply_markup=kb.printer_yes_no())
    elif message.chat.id in db.get_users():
        await message.answer(f"⚔️ Простите, у Вас нет администраторских прав для добавления подразделения", 
                             reply_markup=types.ReplyKeyboardRemove())
    else:
        await message.answer("⚔️ Простите, у Вас нет доступа к боту. Чтобы получить доступ напишите /help", reply_markup=types.ReplyKeyboardRemove())

@dp.message_handler(state=None)
async def non_state(message: types.Message):
    if message.chat.id in db.get_users():
        user_podr = json.loads(db.get_user_info(message.chat.id)[0][6].replace("'", "\""))['podr']
        await message.answer(f"✏️ Напишите {hbold('имя')} и {hbold('фамилия')} или {hbold('штрих-код')} консультанта которого хотите найти\n\n" \
                             f"ℹ️ Так же можно отправить {hbold('голосовое сообщение')} для поиска или {hbold('фото этикетки')} с qr кодом\n" \
                             f"ℹ️ Доступные Вам подразделения: {user_podr}", 
                             reply_markup=kb.get_main_menu_il())
        await Print_state.search.set()

    else:
        await message.answer("⚔️ Простите, у Вас нет доступа к боту. Чтобы получить доступ напишите /help", reply_markup=types.ReplyKeyboardRemove())


@dp.message_handler(state=Print_state.search, content_types=['text', 'photo', 'voice'])
async def search(message: types.Message):
    if message.chat.id in db.get_users():
        user_podr = json.loads(db.get_user_info(message.chat.id)[0][6].replace("'", "\""))['podr']
        if message.content_type == 'text':
            good_search_res = list()
            for p in user_podr:
                db_search = db.get_cons_search(message.text.title(), p)
                if db_search != []:
                    good_search_res.append(db_search[0])
            if len(good_search_res) != 0:
                db.update_search_res(good_search_res, message.chat.id)
                msg = str()
                count = 1
                for s in good_search_res:
                    cons_data = json.loads(str(s).replace("'", "\""))
                    msg = msg + f"{hbold(count)}) {hbold('Имя:')} {hcode(cons_data['firstname'])}\n    " \
                                f"{hbold('Фамилия:')} {hcode(cons_data['lastname'])}\n    " \
                                f"{hbold('ШК:')}  {hcode(cons_data['qr'])}\n    " \
                                f"{hbold('Подразделение:')}  {hcode(cons_data['podr'])}\n"
                    count += 1
                await message.answer(f"🔎 По данному запросу нашлось:\n{msg}\n\n" \
                                    f"ℹ️ Напишите номер консультанта, например: {hcode('1')}", 
                                    reply_markup=kb.get_main_menu_il())
                await Print_state.search_res.set()
            else:
                await message.answer(f"🤷‍♂️ По данному запросу ничего не нашлось.\n\n" \
                                     f"✏️ Напишите {hbold('имя')} и {hbold('фамилия')} или {hbold('штрих-код')} консультанта которого хотите найти\n\n" \
                                     f"ℹ️ Так же можно отправить {hbold('голосовое сообщение')} для поиска или {hbold('фото этикетки')} с qr кодом\n" \
                                     f"ℹ️ Доступные Вам подразделения: {user_podr}", 
                                    reply_markup=kb.get_main_menu_il())
                await Print_state.search.set()
        elif message.content_type == 'voice':
            temp_voice = tempfile.NamedTemporaryFile(prefix="audio_", suffix='.ogg').name
            await message.voice.download(destination_file=temp_voice)
            text = convert.voice_to_text(temp_voice)
            good_search_res = list()
            for p in user_podr:
                db_search = db.get_cons_search(text.title(), p)
                if db_search != []:
                    good_search_res.append(db_search[0])
            if len(good_search_res) != 0:
                db.update_search_res(good_search_res, message.chat.id)
                msg = str()
                count = 1
                for s in good_search_res:
                    cons_data = json.loads(str(s).replace("'", "\""))
                    msg = msg + f"{hbold(count)}) {hbold('Имя:')} {hcode(cons_data['firstname'])}\n    " \
                                f"{hbold('Фамилия:')} {hcode(cons_data['lastname'])}\n    " \
                                f"{hbold('ШК:')}  {hcode(cons_data['qr'])}\n    " \
                                f"{hbold('Подразделение:')}  {hcode(cons_data['podr'])}\n"
                    count += 1
                await message.answer(f"🔎 По данному запросу нашлось:\n{msg}\n\n" \
                                    f"ℹ️ Напишите номер консультанта, например: {hcode('1')}", 
                                    reply_markup=kb.get_main_menu_il())
                await Print_state.search_res.set()
            else:
                await message.answer(f"🤷‍♂️ По данному запросу ничего не нашлось.\n\n" \
                                     f"✏️ Напишите {hbold('имя')} и {hbold('фамилия')} или {hbold('штрих-код')} консультанта которого хотите найти\n\n" \
                                     f"ℹ️ Так же можно отправить {hbold('голосовое сообщение')} для поиска или {hbold('фото этикетки')} с qr кодом" \
                                     f"ℹ️ Доступные Вам подразделения: {user_podr}", 
                                    reply_markup=kb.get_main_menu_il())
                await Print_state.push_search.set()
        elif message.content_type == 'photo':
            temp_photo = tempfile.NamedTemporaryFile(prefix="photo_", suffix='.jpg').name
            await message.photo[-1].download(destination_file=temp_photo)
            qr_data = convert.qr_to_text(temp_photo)
            if qr_data:
                code_num = qr_data.data.decode()
                code_type = qr_data.type
                if code_type == 'QRCODE':
                    if len(code_num) == 13:
                        await message.answer(f"Вы скинули: {code_num}\nПока в разработке...", reply_markup=kb.get_main_menu_il())
                    else:
                        await message.answer(f"🤷‍♂️ Простите, в данном qrcode не правильные данные...", reply_markup=kb.get_main_menu_il())
                else:
                    await message.answer(f"🤷‍♂️ Простите, этот тип штрих кода не определен как qrcode...", reply_markup=kb.get_main_menu_il()) 
            else:
                await message.answer(f"🤷‍♂️ Простите, по данному фото не найден ни один qrcode...", reply_markup=kb.get_main_menu_il())
    else:
        await message.answer("⚔️ Простите, у Вас нет доступа к боту. Чтобы получить доступ напишите /help", reply_markup=types.ReplyKeyboardRemove())


@dp.message_handler(state=Print_state.search_res)
async def search_res(message: types.Message):
    if message.chat.id in db.get_users():
        search_res_db = db.get_search_res(message.chat.id)
        search_res_list = list()
        for s in search_res_db:
            cons_data = json.loads(str(s).replace("'", "\""))
            search_res_list.append(cons_data)
        if str(message.text).isdigit():
            if int(message.text) - 1 >= len(search_res_list[0]) or int(message.text) == 0:
                msg = str()
                count = 1
                for s in search_res_list[0]:
                    msg = msg + f"{hbold(count)}) {hbold('Имя:')} {hcode(s['firstname'])}\n    " \
                                f"{hbold('Фамилия:')} {hcode(s['lastname'])}\n    " \
                                f"{hbold('ШК:')} {hcode(s['qr'])}\n    " \
                                f"{hbold('Подразделение:')} {hcode(s['podr'])}\n"
                    count += 1
                await message.answer(f"🤷‍♂️ Простите, введенное Вами число больше списка.\n{msg}\n\n" \
                                     f"ℹ️ Напишите номер консультанта по списку, например: {hbold('1')}", 
                                     reply_markup=kb.get_main_menu_il())
            else:
                db.update_print_data(search_res_list[0][int(message.text) - 1], message.chat.id)
                await message.answer(f"▶️ Напишите количество этикеток которое хотите распечатать.\n\n" \
                                     f"ℹ️ Например: {hcode('10')}", reply_markup=kb.get_main_menu_il()) 
                await Print_state.quantity.set()
        else:
            msg = str()
            count = 1
            for s in search_res_list[0]:
                msg = msg + f"{hbold(count)}) {hbold('Имя:')} {hcode(s['firstname'])}\n    " \
                            f"{hbold('Фамилия:')} {hcode(s['lastname'])}\n    " \
                            f"{hbold('ШК:')} {hcode(s['qr'])}\n    " \
                            f"{hbold('Подразделение:')} {hcode(s['podr'])}\n"
                count += 1
            await message.answer(f"🤷‍♂️ Простите, нужно укзазать числовое значение.\n\n▶️ Напишите номер консультанта из списка:\n{msg}", 
                                     reply_markup=kb.get_main_menu_il())
    else:
        await message.answer("⚔️ Простите, у Вас нет доступа к боту. Чтобы получить доступ напишите /help", reply_markup=types.ReplyKeyboardRemove())


@dp.message_handler(state=Print_state.quantity)
async def quantity(message: types.Message):
    if message.chat.id in db.get_users():
        print_res_db = db.get_print_res(message.chat.id)[0]
        cons_data = json.loads(str(print_res_db).replace("'", "\""))
        user_podr = json.loads(db.get_user_info(message.chat.id)[0][6].replace("'", "\""))['podr']
        available_printers = list()
        for p in user_podr:
            podr_printer = db.get_printer_info(p)
            if podr_printer != []:
                available_printers.append(db.get_printer_info(p)[0])
        if str(message.text).isdigit():
            db.update_quantity(message.text, message.chat.id )
            quantity = db.get_quantity_res(message.chat.id)[0]
            if len(available_printers) > 1:
                msg = str()
                count = 1
                for p in available_printers:
                    msg = msg + f'{count}) {hbold("Имя")}: {hcode(p[1])}\n    ' \
                                f'{hbold("Подразделение")}: {hcode(p[4])}\n    ' \
                                f'{hbold("Марка принтера")}: {hcode(p[2])}\n    ' \
                                f'{hbold("IP")}: {hcode(p[3])}\n'
                    count += 1
                await Print_state.select_printer.set()
                await message.answer(f"▶️ Для Вас доступны принтеры:\n\n{msg}\n✏️ Напишите номер принтера по списку.\n\nℹ️ Например: 1", 
                                     reply_markup=kb.get_main_menu_il())
            elif len(available_printers) == 0:
                    await message.answer(f"🤷‍♂️ Простите, для вашего подразделения нет добавленных принтеров, обратитесь к администратору бота", 
                                        reply_markup=kb.get_main_menu_il())
            else:
                if int(message.text) < 500:
                    db.update_quantity(message.text, message.chat.id )
                    await message.answer(f"{hbold('Имя:')} {hcode(cons_data['firstname'])}\n" \
                                         f"{hbold('Фамилия:')} {hcode(cons_data['lastname'])}\n" \
                                         f"{hbold('ШК:')}  {hcode(cons_data['qr'])}\n" \
                                         f"{hbold('Подразделение:')}  {hcode(cons_data['podr'])}\n" \
                                         f"{hbold('Количество:')} {hcode(quantity)}\n" \
                                         f"{hbold('Принтер:')} {hcode(available_printers[0][1])}\n\n" \
                                         f"Печатаем?", 
                                         reply_markup=kb.print_menu())
                    db.update_print_name(available_printers[0][1], message.chat.id)
                    await Print_state.print.set()
                else:
                    await message.answer(f"🤷‍♂️ Простите, что то Вы указали слишком большое количество этикеток.\n\n" \
                                         f"ℹ️ Количество не должно привышать {hcode('500')}", 
                                         reply_markup=kb.get_main_menu_il())
        else:
            await message.answer(f"🤷‍♂️ Простите, нужно укзазать количество этикеток {hbold('числовом')} формате\n\nℹ️ Например: {hcode('10')}", 
                                 reply_markup=kb.get_main_menu_il())
    else:
        await message.answer("⚔️ Простите, у Вас нет доступа к боту. Чтобы получить доступ напишите /help", reply_markup=types.ReplyKeyboardRemove())


@dp.message_handler(state=Print_state.select_printer)
async def print_tiket(message: types.Message):
    if message.chat.id in db.get_users():
        quantity = db.get_quantity_res(message.chat.id)[0]
        print_res_db = db.get_print_res(message.chat.id)[0]
        cons_data = json.loads(str(print_res_db).replace("'", "\""))
        user_podr = json.loads(db.get_user_info(message.chat.id)[0][6].replace("'", "\""))['podr']
        available_printers = list()
        for p in user_podr:
            podr_printer = db.get_printer_info(p)
            if podr_printer != []:
                available_printers.append(db.get_printer_info(p)[0])
        if str(message.text).isdigit():
            if int(message.text) <= len(available_printers) and int(message.text) != 0:
                await message.answer(f"{hbold('Имя:')} {hcode(cons_data['firstname'])}\n" \
                                         f"{hbold('Фамилия:')} {hcode(cons_data['lastname'])}\n" \
                                         f"{hbold('ШК:')}  {hcode(cons_data['qr'])}\n" \
                                         f"{hbold('Подразделение:')}  {hcode(cons_data['podr'])}\n" \
                                         f"{hbold('Количество:')} {hcode(quantity)}\n" \
                                         f"{hbold('Принтер:')} {hcode(available_printers[int(message.text) - 1][1])}\n\n" \
                                         f"Печатаем?", 
                                         reply_markup=kb.print_menu())
                db.update_print_name(available_printers[int(message.text) - 1][1], message.chat.id)
                await Print_state.print.set()
            else:
                msg = str()
                count = 1
                for p in available_printers:
                    msg = msg + f'{count}) {hbold("Имя")}: {hcode(p[1])}\n    ' \
                                f'{hbold("Подразделение")}: {hcode(p[4])}\n    ' \
                                f'{hbold("Марка принтера")}: {hcode(p[2])}\n    ' \
                                f'{hbold("IP")}: {hcode(p[3])}\n'
                    count += 1
                await message.answer(f"🤷‍♂️ Простите, нужно выбрать принтер из списка в пределах списка\n" \
                                     f"Доступные принтеры:\n\n{msg}\n✏️ Напишите номер принтера по списку", 
                                     reply_markup=kb.get_main_menu_il())
    else:
        await message.answer("⚔️ Простите, у Вас нет доступа к боту. Чтобы получить доступ напишите /help", reply_markup=types.ReplyKeyboardRemove())


@dp.message_handler(state=Print_state.print)
async def print_tiket(message: types.Message):
    if message.chat.id in db.get_users():
        user_podr = json.loads(db.get_user_info(message.chat.id)[0][6].replace("'", "\""))['podr']
        print_res_db = db.get_print_res(message.chat.id)[0]
        print_data = json.loads(str(print_res_db).replace("'", "\""))
        printer_name = db.get_print_name(message.chat.id)
        quantity = db.get_quantity_res(message.chat.id)[0]
        if message.text == "✅ Да":
                    fullname = print_data['firstname'] + ' ' + print_data['lastname']
                    cons_qr = print_data['qr']
                    pdf_file = pdf.get_pdf(data=cons_qr, text=fullname, is_micro=micro_qr_print)
                    for q in range(quantity):
                        printers.print(pdf_file=pdf_file, printer_name=printer_name[0])
                    await message.answer(f" ✅Отправлено на печать!\n\n" \
                                         f"{hbold('Имя:')} {hcode(print_data['firstname'])}\n" \
                                         f"{hbold('Фамилия:')} {hcode(print_data['lastname'])}\n" \
                                         f"{hbold('ШК:')}  {hcode(print_data['qr'])}\n" \
                                         f"{hbold('Подразделение:')}  {hcode(print_data['podr'])}\n" \
                                         f"{hbold('Количество:')} {hcode(quantity)}\n" \
                                         f"{hbold('Принтер:')} {hcode(printer_name[0])}\n\n" \
                                         f"Печатаем еще раз?", 
                                         reply_markup=kb.print_menu())
        elif message.text == "▶️ Да пробно 1":
                fullname = print_data['firstname'] + ' ' + print_data['lastname']
                cons_qr = print_data['qr']
                pdf_file = pdf.get_pdf(data=cons_qr, text=fullname, is_micro=micro_qr_print)
                printers.print(pdf_file=pdf_file, printer_name=printer_name[0])
                await message.answer(f"✅ Отправлено на печать!\n\n" \
                                     f"{hbold('Имя:')} {hcode(print_data['firstname'])}\n" \
                                     f"{hbold('Фамилия:')} {hcode(print_data['lastname'])}\n" \
                                     f"{hbold('ШК:')}  {hcode(print_data['qr'])}\n" \
                                     f"{hbold('Подразделение:')}  {hcode(print_data['podr'])}\n" \
                                     f"{hbold('Количество:')} {hcode(quantity)}\n" \
                                     f"{hbold('Принтер:')} {hcode(printer_name[0])}\n\n" \
                                     f"Печатаем еще раз?", 
                                     reply_markup=kb.print_menu())
        elif message.text == "❌ Нет, в главное меню":
            await message.answer(f"✏️ Напишите {hbold('имя')} и {hbold('фамилия')} или {hbold('штрих-код')} консультанта которого хотите найти\n\n" \
                                f"ℹ️ Так же можно отправить {hbold('голосовое сообщение')} для поиска или {hbold('фото этикетки')} с qr кодом" \
                                f"ℹ️ Доступные Вам подразделения: {user_podr}", 
                                reply_markup=kb.get_main_menu_il())
            await Print_state.search.set()
        else:
            await message.answer(f"Простите я Вас не понимаю\n" \
                                f"{hbold('Имя:')} {hcode(print_data['firstname'])}\n" \
                                f"{hbold('Фамилия:')} {hcode(print_data['lastname'])}\n" \
                                f"{hbold('ШК:')}  {hcode(print_data['qr'])}\n" \
                                f"{hbold('Подразделение:')}  {hcode(print_data['podr'])}\n" \
                                f"{hbold('Количество:')} {hcode(message.text)}\n" \
                                f"{hbold('Принтер:')} {hcode(printer_name[0])}\n\n" \
                                f"Печатаем?", 
                                reply_markup=kb.print_menu())
    else:
        await message.answer("⚔️ Простите, у Вас нет доступа к боту. Чтобы получить доступ напишите /help", reply_markup=types.ReplyKeyboardRemove())


def main():
    db.add_superuser(super_administrator)
    executor.start_polling(dp, skip_updates=True)


if __name__ == "__main__":
    main()