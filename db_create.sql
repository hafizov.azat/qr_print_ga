CREATE TABLE "podr" (
	"id"	INTEGER NOT NULL UNIQUE,
	"discription"	TEXT,
	PRIMARY KEY("id")
);

CREATE TABLE "users" (
	"tg_id"	INTEGER NOT NULL UNIQUE,
	"tmp_search_res"	INTEGER,
	"tmp_print_data"	TEXT,
	"tmp_quantity"	INTEGER,
	"tmp_print_name"	TEXT,
	"is_admin"	BLOB,
	"available_podr"	TEXT,
	"description"	TEXT,
	PRIMARY KEY("tg_id")
);

CREATE TABLE "consultants" (
	"id"	INTEGER NOT NULL UNIQUE,
	"firstname"	TEXT,
	"lastname"	TEXT,
	"qr"	INTEGER,
	"podr_id"	INTEGER,
	PRIMARY KEY("id"),
	FOREIGN KEY("podr_id") REFERENCES "podr"("id")
);

CREATE TABLE "printers" (
	"id"	INTEGER NOT NULL UNIQUE,
	"cups_printer_name"	TEXT,
	"vendor_name"	TEXT,
	"ip"	INTEGER,
	"podr_id"	INTEGER,
	FOREIGN KEY("podr_id") REFERENCES "podr"("id"),
	PRIMARY KEY("id")
);

CREATE TABLE "temp_add_podr" (
	"tg_id"	INTEGER UNIQUE,
	"podr_id"	INTEGER,
	"printer_type"	TEXT,
	"printer_ip"	TEXT,
	"printer_smb_name"	TEXT,
	"printer_cred"	TEXT
);