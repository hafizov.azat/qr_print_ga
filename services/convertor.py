import speech_recognition as sr
import tempfile
import os
import cv2
from pyzbar import pyzbar


def voice_to_text(path_to_audio):
    temp_wav = tempfile.NamedTemporaryFile(prefix="audio_", suffix=".wav").name
    os.system(f'ffmpeg -i {path_to_audio} -acodec pcm_s16le -ar 8000 -ab 250000  -ac 1  -y  {temp_wav}')
    r = sr.Recognizer()
    with sr.AudioFile(temp_wav) as source:
        audio_data = r.record(source)
        recognized_text = r.recognize_google(audio_data, language="ru-RU")
    os.remove(path_to_audio)
    os.remove(temp_wav)
    recognized_text = recognized_text.split(' ')
    text = str()
    for t in recognized_text:
        text = text + t.title()
    return text


def qr_to_text(img):
    image = cv2.imread(img)
    decoded_objects = pyzbar.decode(image)
    try:
        return decoded_objects[0]
    except:
        return False
    finally:
        os.remove(img)
