import sqlite3


def get_users():
    conn = sqlite3.connect('database.db')
    cur = conn.cursor()
    cur.execute("SELECT tg_id FROM users")
    data = cur.fetchall()
    cur.close()
    conn.close()
    tg_ids = list()
    for d in data:
        tg_ids.append(d[0])
    return tg_ids


def get_admins():
    conn = sqlite3.connect('database.db')
    cur = conn.cursor()
    cur.execute("SELECT * FROM users")
    data = cur.fetchall()
    cur.close()
    conn.close()
    tg_ids = list()
    for d in data:
        if d[5] == 'True':
            tg_ids.append(d[0])
    return tg_ids


def get_user_info(tg_id):
    conn = sqlite3.connect('database.db')
    cur = conn.cursor()
    cur.execute("SELECT * FROM users WHERE tg_id = ?", (tg_id,))
    data = cur.fetchall()
    cur.close()
    conn.close()
    return data


def add_user(tg_id, available_podr, is_admin=False):
    conn = sqlite3.connect('database.db')
    cur = conn.cursor()
    try:
        cur.execute("INSERT INTO users(tg_id, is_admin, available_podr) VALUES (?, ?, ?);", (tg_id, str(is_admin), str(available_podr),))
        conn.commit()
        cur.close()
        conn.close()
        return "Added"
    except:
        print(is_admin)
        cur.execute("UPDATE users SET available_podr = ?, is_admin = ? WHERE tg_id = ?", (str(available_podr), str(is_admin), tg_id,))
        conn.commit()
        cur.close()
        conn.close()
        return "Updated"   


def del_user(tg_id):
    conn = sqlite3.connect('database.db')
    cur = conn.cursor()
    cur.execute(f"DELETE FROM users WHERE tg_id = ?", (tg_id,))
    conn.commit()   
    cur.close()
    conn.close()


def get_printer_info(podr):
    conn = sqlite3.connect('database.db')
    cur = conn.cursor()
    cur.execute("SELECT * FROM printers WHERE podr_id = ?", (podr,))
    data = cur.fetchall()
    cur.close()
    conn.close()
    return data


def update_search_res(search_res, tg_id):
    conn = sqlite3.connect('database.db')
    cur = conn.cursor()
    cur.execute("UPDATE users SET tmp_search_res = ? WHERE tg_id = ?", (str(search_res), tg_id,))
    conn.commit()
    cur.close()
    conn.close()


def get_search_res(tg_id):
    conn = sqlite3.connect('database.db')
    cur = conn.cursor()
    cur.execute("SELECT tmp_search_res FROM users WHERE tg_id = ?", (tg_id,))
    data = cur.fetchall()
    cur.close()
    conn.close()
    return data[0]


def get_print_res(tg_id):
    conn = sqlite3.connect('database.db')
    cur = conn.cursor()
    cur.execute("SELECT tmp_print_data FROM users WHERE tg_id = ?", (tg_id,))
    data = cur.fetchall()
    cur.close()
    conn.close()
    return data[0]


def update_print_data(print_data, tg_id):
    conn = sqlite3.connect('database.db')
    cur = conn.cursor()
    cur.execute("UPDATE users SET tmp_print_data = ? WHERE tg_id = ?", (str(print_data), tg_id,))
    conn.commit()
    cur.close()
    conn.close()


def get_print_name(tg_id):
    conn = sqlite3.connect('database.db')
    cur = conn.cursor()
    cur.execute("SELECT tmp_print_name FROM users WHERE tg_id = ?", (tg_id,))
    data = cur.fetchall()
    cur.close()
    conn.close()
    return data[0]


def update_print_name(print_name, tg_id):
    conn = sqlite3.connect('database.db')
    cur = conn.cursor()
    cur.execute("UPDATE users SET tmp_print_name = ? WHERE tg_id = ?", (str(print_name), tg_id,))
    conn.commit()
    cur.close()
    conn.close()


def get_quantity_res(tg_id):
    conn = sqlite3.connect('database.db')
    cur = conn.cursor()
    cur.execute("SELECT tmp_quantity FROM users WHERE tg_id = ?", (tg_id,))
    data = cur.fetchall()
    cur.close()
    conn.close()
    return data[0]


def update_quantity(quantity, tg_id):
    conn = sqlite3.connect('database.db')
    cur = conn.cursor()
    cur.execute("UPDATE users SET tmp_quantity = ? WHERE tg_id = ?", (quantity, tg_id,))
    conn.commit()
    cur.close()
    conn.close()


def get_podr_id():
    conn = sqlite3.connect('database.db')
    cur = conn.cursor()
    cur.execute("SELECT * FROM podr")
    data = cur.fetchall()
    cur.close()
    conn.close()
    podr_list = list()
    for d in data:
        podr_list.append(d[0])
    return podr_list


def get_cons_search(query, podr):
    query = str(query)
    conn = sqlite3.connect('database.db')
    cur = conn.cursor()
    cur.execute("SELECT * FROM consultants")
    data = cur.fetchall()
    cur.close()
    conn.close()
    good_res = list()
    if not query.isdigit():
        query = query.split(' ')
        if len(query) > 1:
            one_word = query[0]
            two_word = query[1]
            for d in data:
                if one_word == d[1] and two_word == d[2]:
                    if podr ==d[4]:
                        good_res.append({'id': d[0], 'firstname': d[1], 'lastname': d[2], 'qr': d[3], 'podr': d[4]})
                elif one_word == d[2] and two_word == d[1]:
                    if podr ==d[4]:
                        good_res.append({'id': d[0], 'firstname': d[1], 'lastname': d[2], 'qr': d[3], 'podr': d[4]})
        else:
            for d in data:
                if d[1] in query[0]:
                    if podr ==d[4]:
                        good_res.append({'id': d[0], 'firstname': d[1], 'lastname': d[2], 'qr': d[3], 'podr': d[4]})
                elif d[2] in query[0]:
                    if podr ==d[4]:
                        good_res.append({'id': d[0], 'firstname': d[1], 'lastname': d[2], 'qr': d[3], 'podr': d[4]})
        
    else:
        for d in data:
            if podr ==d[4]:
                if query == str(d[3]):
                    good_res.append({'id': d[0], 'firstname': d[1], 'lastname': d[2], 'qr': d[3], 'podr': d[4]})
    return good_res


def add_cons(firstname, lastname, qr, podr):
    conn = sqlite3.connect('database.db')
    cur = conn.cursor()
    cur.execute("SELECT * FROM consultants")
    test = True
    consultants = cur.fetchall()
    for c in consultants:
        if str(c[3]) == str(qr) and str(c[4]) == str(podr):
            test = False
    if test:
        cur.execute("INSERT INTO consultants(firstname, lastname, qr, podr_id) VALUES (?, ?, ?, ?);", (firstname, lastname, qr, podr,))
        conn.commit()
        return "Added"
    else:
        cur.execute("UPDATE consultants SET firstname = ?, lastname = ?, podr_id = ? WHERE qr = ?;", (firstname, lastname, podr, qr,))
        conn.commit()
        return "Updated"



def tmp_update_podr_id(tg_id, podr_id):
    conn = sqlite3.connect('database.db')
    cur = conn.cursor()
    try:
        cur.execute("INSERT INTO temp_add_podr(tg_id, podr_id) VALUES (?, ?);", (tg_id, podr_id,))
        conn.commit()
    except:
        cur.execute("UPDATE temp_add_podr SET podr_id = ? WHERE tg_id = ?;", (podr_id, tg_id,))
        conn.commit()
    finally:
        cur.close()
        conn.close()


def tmp_get_podr_id(tg_id):
    conn = sqlite3.connect('database.db')
    cur = conn.cursor()
    cur.execute("SELECT podr_id FROM temp_add_podr WHERE tg_id = ?", (tg_id,))
    data = cur.fetchall()
    cur.close()
    conn.close()
    return data[0][0]


def tmp_update_printer_type(tg_id, printer_type):
    conn = sqlite3.connect('database.db')
    cur = conn.cursor()
    try:
        cur.execute("INSERT INTO temp_add_podr(tg_id, printer_type) VALUES (?, ?);", (tg_id, printer_type,))
        conn.commit()
    except:
        cur.execute("UPDATE temp_add_podr SET printer_type = ? WHERE tg_id = ?;", (printer_type, tg_id,))
        conn.commit()
    finally:
        cur.close()
        conn.close()


def tmp_get_printer_type(tg_id):
    conn = sqlite3.connect('database.db')
    cur = conn.cursor()
    cur.execute("SELECT printer_type FROM temp_add_podr WHERE tg_id = ?", (tg_id,))
    data = cur.fetchall()
    cur.close()
    conn.close()
    return data[0][0]


def tmp_update_printer_ip(tg_id, printer_ip):
    conn = sqlite3.connect('database.db')
    cur = conn.cursor()
    try:
        cur.execute("INSERT INTO temp_add_podr(tg_id, printer_ip) VALUES (?, ?);", (tg_id, printer_ip,))
        conn.commit()
    except:
        cur.execute("UPDATE temp_add_podr SET printer_ip = ? WHERE tg_id = ?;", (printer_ip, tg_id,))
        conn.commit()
    finally:
        cur.close()
        conn.close()


def tmp_get_printer_ip(tg_id):
    conn = sqlite3.connect('database.db')
    cur = conn.cursor()
    cur.execute("SELECT printer_ip FROM temp_add_podr WHERE tg_id = ?", (tg_id,))
    data = cur.fetchall()
    cur.close()
    conn.close()
    return data[0][0]


def tmp_update_printer_ip(tg_id, printer_ip):
    conn = sqlite3.connect('database.db')
    cur = conn.cursor()
    try:
        cur.execute("INSERT INTO temp_add_podr(tg_id, printer_ip) VALUES (?, ?);", (tg_id, printer_ip,))
        conn.commit()
    except:
        cur.execute("UPDATE temp_add_podr SET printer_ip = ? WHERE tg_id = ?;", (printer_ip, tg_id,))
        conn.commit()
    finally:
        cur.close()
        conn.close()


def tmp_get_printer_smb_name(tg_id):
    conn = sqlite3.connect('database.db')
    cur = conn.cursor()
    cur.execute("SELECT printer_smb_name FROM temp_add_podr WHERE tg_id = ?", (tg_id,))
    data = cur.fetchall()
    cur.close()
    conn.close()
    return data[0][0]


def tmp_update_printer_smb_name(tg_id, smb_name):
    conn = sqlite3.connect('database.db')
    cur = conn.cursor()
    try:
        cur.execute("INSERT INTO temp_add_podr(tg_id, printer_smb_name) VALUES (?, ?);", (tg_id, smb_name,))
        conn.commit()
    except:
        cur.execute("UPDATE temp_add_podr SET printer_smb_name = ? WHERE tg_id = ?;", (smb_name, tg_id,))
        conn.commit()
    finally:
        cur.close()
        conn.close()


def tmp_get_printer_cred(tg_id):
    conn = sqlite3.connect('database.db')
    cur = conn.cursor()
    cur.execute("SELECT printer_cred FROM temp_add_podr WHERE tg_id = ?", (tg_id,))
    data = cur.fetchall()
    cur.close()
    conn.close()
    return data[0][0]


def tmp_update_printer_cred(tg_id, cred):
    conn = sqlite3.connect('database.db')
    cur = conn.cursor()
    try:
        cur.execute("INSERT INTO temp_add_podr(tg_id, printer_cred) VALUES (?, ?);", (tg_id, cred))
        conn.commit()
    except:
        cur.execute("UPDATE temp_add_podr SET printer_cred = ? WHERE tg_id = ?;", (cred, tg_id,))
        conn.commit()
    finally:
        cur.close()
        conn.close()


def tmp_pord_data_clear(tg_id):
    conn = sqlite3.connect('database.db')
    cur = conn.cursor()
    cur.execute(f"DELETE FROM temp_add_podr WHERE tg_id = ?", (tg_id,))
    conn.commit()   
    cur.close()
    conn.close()


def create_podr(podr, printer_type, printer_ip, printer_name):
    print(podr)
    conn = sqlite3.connect('database.db')
    cur = conn.cursor()
    try:
        cur.execute(f"INSERT INTO podr(id) VALUES ({podr});")
        conn.commit()
        cur.execute("INSERT INTO printers(cups_printer_name, vendor_name, ip, podr_id) VALUES (?, ?, ?, ?);", (printer_name, printer_type, printer_ip, podr))
        conn.commit()
        return "Added"
    except:
        cur.execute("UPDATE printers SET cups_printer_name = ?,  vendor_name = ?, ip = ? WHERE podr_id = ?;", (printer_name, printer_type, printer_ip, podr))
        conn.commit()
        return "Updated"
    finally:
        cur.close()
        conn.close()

def add_superuser(tg_id):
    conn = sqlite3.connect('database.db')
    cur = conn.cursor()
    podr = get_podr_id()
    available_podr = "{" + "'podr': " + str(podr) + "}"
    try:
        cur.execute("INSERT INTO users(tg_id, is_admin, available_podr, description) VALUES (?, ?, ?, ?);", (tg_id, "True", available_podr, "SuperAdmin"))
        conn.commit()
    except:
        cur.execute("UPDATE users SET is_admin = ?, available_podr = ?, description = ? WHERE tg_id = ?;", ("True", available_podr, "SuperAdmin", tg_id,))
        conn.commit()
    finally:
        cur.close()
        conn.close()