from aiogram import types


def get_main_menu_il():
    main_menu_inline_kb = types.InlineKeyboardMarkup()
    il_kb = types.InlineKeyboardButton(text='Выйти в главное меню', callback_data="get_main_menu")
    main_menu_inline_kb.add(il_kb)
    return main_menu_inline_kb

def print_menu():
    kb = types.ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
    bt = ["✅ Да", "❌ Нет, в главное меню", "▶️ Да пробно 1"]
    kb.add(*bt)
    return kb


def printer_type():
    kb = types.ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
    bt = ["Citizen", "Zebra"]
    kb.add(*bt)
    return kb

def printer_yes_no():
    kb = types.ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
    bt = ["✅ Да", "❌ Нет"]
    kb.add(*bt)
    return kb