from reportlab.pdfgen.canvas import Canvas
from reportlab.lib.units import cm
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.lib import utils
import tempfile
import segno
import os


def get_pdf(data, text, is_micro=True):
    temp_qr = tempfile.NamedTemporaryFile(prefix="qr_", suffix=".png").name
    temp_pdf = tempfile.NamedTemporaryFile(prefix="pdf_", suffix=".pdf").name
    first_name = text.split(' ')[0]
    while len(first_name) != 11:
        if len(first_name) > 11:
            break
        first_name = ' ' + first_name
    try:
        last_name = text.split(' ')[1]
        while len(last_name) != 11:
            if len(last_name) > 11:
                break
            last_name = ' ' + last_name
    except:
        last_name = ''
    qrcode_micro = segno.make(data, micro=is_micro)
    qrcode_micro.save(temp_qr, border=2, scale=8)
    img = utils.ImageReader(temp_qr)
    img_width, img_height = img.getSize()
    aspect = img_height / float(img_width)
    canvas = Canvas(temp_pdf, pagesize=(3 * cm, 2 * cm))
    pdfmetrics.registerFont(TTFont('roboto-regular', 'font/roboto-regular.ttf'))
    canvas.setFont('roboto-regular', 8)
    canvas.drawImage(temp_qr, 33, 3, width=50, height=(50 * aspect))
    canvas.saveState()
    canvas.rotate(90)
    canvas.drawString(3, -10, first_name)
    canvas.drawString(3, -20, last_name)
    canvas.restoreState()
    canvas.save()
    os.remove(temp_qr)
    return temp_pdf
