import cups


def print(pdf_file, printer_name):
    conn = cups.Connection()
    conn.printFile(printer_name, pdf_file, "Python_Status_print", {})


def get_printers():
    conn = cups.Connection()
    printers = conn.getPrinters()
    return printers
